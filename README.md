# 👋 Hi, I'm Jan Urbaś 

💻 Experienced Full Stack Developer | .NET | Angular | Azure DevOps \
🚀 I'm passionate about creating efficient and scalable solutions that brings value to the business.

## 🔧 Technologies & Tools
![.NET](https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white)
![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![Azure DevOps](https://img.shields.io/badge/Azure_DevOps-0078D7?style=for-the-badge&logo=azure-devops&logoColor=white)
![Docker](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)
![RabbitMQ](https://img.shields.io/badge/RabbitMQ-FF6600?style=for-the-badge&logo=rabbitmq&logoColor=white)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-336791?style=for-the-badge&logo=postgresql&logoColor=white)
![Microsoft SQL Server](https://img.shields.io/badge/Microsoft_SQL_Server-CC2927?style=for-the-badge&logo=microsoft-sql-server&logoColor=white)
![Git](https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=git&logoColor=white)
![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)
![Powershell](https://img.shields.io/badge/Powershell-2CA5E0?style=for-the-badge&logo=powershell&logoColor=white)
![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)

## 🎯 What I Bring To The Table:
- **Backend & Frontend Mastery**: Merging .NET and Angular to deliver end-to-end products.
- **DevOps Mindset**: Setting up pipelines, automating deployments, and ensuring smooth releases.
- **Collaborative Spirit**: Communicating effectively across teams to build robust software.
- **Continuous Learning**: Staying ahead of trends in software engineering and best practices.

## 📌 Featured Projects:

- 2024 - [Smart Fridge](https://gitlab.com/Tymisko/fridge) - System to manage diet and shopping lists in a smart way.
- 2023 - [EcoLife.API](https://gitlab.com/Tymisko/ecolife.api) - API for EcoLife mobile application that educates about ecology and sustainability. Mobile application encourages users to change their lifestyle.
- 2022 - [StudentAdminPortal-API](https://gitlab.com/Tymisko/StudentAdminPortal-API) - API for managing student data.
- 2022 - [StudentAdminPortal-UI](https://gitlab.com/Tymisko/StudentAdminPortal-UI) - UI for managing student data.

## 📫 How to reach me:

[![Email](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:janurbas39@gmail.com)
[![Linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/jan-urbaś/)
[![Discord](https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white)](https://discordapp.com/users/248355897728827393)
